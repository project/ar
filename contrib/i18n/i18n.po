# LANGUAGE translation of Drupal (root)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  i18n.admin.inc,v 1.2.2.7 2009/01/21 13:08:35 jareyero
#  i18n.pages.inc,v 1.1.2.2 2009/04/27 14:22:28 jareyero
#  i18n.module,v 1.41.2.34 2009/06/12 11:33:15 jareyero
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-06-26 01:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.6.0\n"

#: i18n.admin.inc:21
msgid "Content selection"
msgstr ""

#: i18n.admin.inc:27
msgid "Content selection mode"
msgstr ""

#: i18n.admin.inc:30
msgid "Determines which content to show depending on the current page language and the default language of the site."
msgstr ""

#: i18n.admin.inc:36
#, fuzzy
msgid "Content translation links"
msgstr "ترجمة المحتوى"

#: i18n.admin.inc:40
msgid "Hide content translation links"
msgstr ""

#: i18n.admin.inc:41
msgid "Hide the links to translations in content body and teasers. If you choose this option, switching language will only be available from the language switcher block."
msgstr ""

#: i18n.admin.inc:46
msgid "Switch interface for translating"
msgstr ""

#: i18n.admin.inc:48
msgid "Switch interface language to fit node language when creating or editing a translation. If not checked the interface language will be independent from node language."
msgstr ""

#: i18n.admin.inc:56
msgid "Current language and language neutral."
msgstr ""

#: i18n.admin.inc:57
msgid "Mixed current language (if available) or default language (if not) and language neutral."
msgstr ""

#: i18n.admin.inc:58
msgid "Only default language and language neutral."
msgstr ""

#: i18n.admin.inc:59
msgid "Only current language."
msgstr ""

#: i18n.admin.inc:60
msgid "All content. No language conditions apply."
msgstr ""

#: i18n.pages.inc:29
msgid "Title"
msgstr "العنوان"

#: i18n.pages.inc:44
msgid "Published"
msgstr "منشور"

#: i18n.pages.inc:44
msgid "Not published"
msgstr "لم ينشر"

#: i18n.pages.inc:45
msgid "outdated"
msgstr "قديمة"

#: i18n.pages.inc:47
msgid "<strong>@language_name</strong> (source)"
msgstr "<strong>@language_name</strong> (الأصل)"

#: i18n.pages.inc:52
msgid "n/a"
msgstr "غير متوفر"

#: i18n.pages.inc:54
msgid "add translation"
msgstr "أضف ترجمة"

#: i18n.pages.inc:56
msgid "Not translated"
msgstr "غير مُتَرجَمة"

#: i18n.pages.inc:61
msgid "Translations of %title"
msgstr "ترجمات %title"

#: i18n.pages.inc:78
msgid "Select translations for %title"
msgstr ""

#: i18n.pages.inc:81
msgid "Alternatively, you can select existing nodes as translations of this one or remove nodes from this translation set. Only nodes that have the right language and don't belong to other translation set will be available here."
msgstr ""

#: i18n.pages.inc:175
msgid "Not found"
msgstr ""

#: i18n.pages.inc:192
msgid "Node title mismatch. Please check your selection."
msgstr ""

#: i18n.pages.inc:204
msgid "Found no valid post with that title: %title"
msgstr ""

#: i18n.pages.inc:145
msgid "Added a node to the translation set."
msgid_plural "Added @count nodes to the translation set."
msgstr[0] ""
msgstr[1] ""

#: i18n.pages.inc:150
msgid "Removed a node from the translation set."
msgid_plural "Removed @count nodes from the translation set."
msgstr[0] ""
msgstr[1] ""

#: i18n.module:51
msgid "This module improves support for multilingual content in Drupal sites:"
msgstr ""

#: i18n.module:53
msgid "Shows content depending on page language."
msgstr ""

#: i18n.module:54
msgid "Handles multilingual variables."
msgstr ""

#: i18n.module:55
msgid "Extended language option for chosen content types. For these content types transations will be allowed for all defined languages, not only for enabled ones."
msgstr ""

#: i18n.module:56
msgid "Provides a block for language selection and two theme functions: <em>i18n_flags</em> and <em>i18n_links</em>."
msgstr ""

#: i18n.module:58
msgid "This is the base module for several others adding different features:"
msgstr ""

#: i18n.module:60
msgid "Multilingual menu items."
msgstr ""

#: i18n.module:61
msgid "Multilingual taxonomy adds a language field for taxonomy vocabularies and terms."
msgstr ""

#: i18n.module:68
msgid "To enable multilingual support for specific content types go to <a href=\"@configure_content_types\">configure content types</a>."
msgstr ""

#: i18n.module:363
msgid "Language neutral"
msgstr "اسم اللغة"

#: i18n.module:544
msgid "Extended language support"
msgstr ""

#: i18n.module:547
msgid "If enabled, all defined languages will be allowed for this content type in addition to only enabled ones. This is useful to have more languages for content than for the interface."
msgstr ""

#: i18n.module:620
msgid "This is a multilingual variable."
msgstr ""

#: i18n.module:636
msgid "Reset to defaults"
msgstr ""

#: i18n.module:882
msgid "Normal - All enabled languages will be allowed."
msgstr ""

#: i18n.module:883
msgid "Extended - All defined languages will be allowed."
msgstr ""

#: i18n.module:884
msgid "Extended, but not displayed - All defined languages will be allowed for input, but not displayed in links."
msgstr ""

#: i18n.module:586
#, fuzzy
msgid "administer all languages"
msgstr "يدير اللغات"

#: i18n.module:586
msgid "administer translations"
msgstr ""

#: i18n.module:79;89
msgid "Multilingual system"
msgstr ""

#: i18n.module:80;90
msgid "Configure extended options for multilingual content and translations."
msgstr ""

#: i18n.module:99
msgid "Node title autocomplete"
msgstr ""
